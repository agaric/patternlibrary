module.exports = function (grunt) {
    grunt.initConfig({
        sass: {
          dev: {
            options: {
              style: 'compact'
            },
            files: {
              'agaric/css/agaric.css': 'agaric/sass/agaric.scss'
            }
          }
        },
        assemble: {
              dev: {
                options: {
                    flatten: true,
                    data: 'static-layouts/design-source/data/*.{json,yml}',
                    layout: false,
                    partials: 'static-layouts/design-source/templates/**/*.hbs'
                },
                files: [
                    {
                      expand: true,
                      cwd: 'static-layouts/design-source/pages',
                      src: '**/*.{hbs,md}',
                      dest: 'static-layouts/design-output/'
                    }
                ]
            }
        },
        clean: ['static-layouts/design-output/*.html'],
        connect: {
            preview: {
                options: {
                    base: ['./'],
                    port: 9000,
                    hostname: 'localhost',
                    keepalive: false,
                    livereload: 35729,
                    open: 'http://0.0.0.0:9000/static-layouts/design-output'
                }
            }
        },
        watch: {
            templates: {
                files: [
                  'static-layouts/design-source/templates/**/*.{hbs,md}',
                  'static-layouts/design-source/pages/**/*.{hbs,md}',
                  'static-layouts/design-source/data/**/*.json',
                  'agaric/sass/**/*.scss',
                ],
                tasks: ['assemble','sass:dev']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.preview.options.livereload %>'
                },
                files: ['static-layouts/design-output/**.*']
            }
        },
        shell: {
            target: {
                command: './node_modules/.bin/kss --config styleguide/_source/styleguide-config.json --verbose'
            }
        }
    });

    grunt.loadNpmTasks('assemble');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-kss');

    grunt.registerTask('kss',['shell']);
    grunt.registerTask('build', ['clean', 'assemble', 'sass:dev']);
    grunt.registerTask('server', ['build','connect','watch']);
    grunt.registerTask('default', ['server']);
};
